package model

import (
	"github.com/go-xorm/xorm"
	"github.com/labstack/gommon/log"
	"time"
)

// 订单表
type Order struct {
	Id                int64
	CustomerId        int       `xorm:"notnull"`             // 客户 ID
	AddresseeId       int       `xorm:"notnull"`             // 收件人 ID
	AddresseeName     string    `xorm:"notnull"`             // 收件人姓名，拷贝
	AddresseePostCode string    `xorm:"notnull default('')"` // 收件人邮编，拷贝
	AddresseeAddress  string    `xorm:"notnull"`             // 收件地址，拷贝，address 表中的地址仅仅作为选择的时候使用。
	CreatedAt         time.Time `xorm:"created"`
	UpdatedAt         time.Time `xorm:"updated"`
}

// 接口返回
type OrderResult struct {
	Id               int             `json:"id"`
	Customer         CustomerResult  `json:"customer"`
	AddresseeName    string          `json:"addressee_name"`
	AddresseeTel     string          `json:"addressee_tel"`
	AddresseeAddress string          `json:"addressee_address"`
	Express          []ExpressResult `json:"express"`
	CreatedAt        int64           `json:"created_at"`
	UpdatedAt        int64           `json:"updated_at"`
}

// 创建
func (o *Order) Create(customerId int, AddresseeName, AddresseeTel, AddresseeAddress string, expressCode []string) (int, error) {
	// 确认客户是否存在
	has, err

	// 创建订单

	// 创建物流订单

	return 0, nil
}

// 删除
func (o *Order) DeleteMulti(ids []int) error {
	// 批量删除订单
	_, err := GetDB().Transaction(func(s *xorm.Session) (interface{}, error) {
		// 删除 express 表中数据
		_, err := s.In("order_id", ids).Delete(&Express{})
		if err != nil {
			log.Printf("[db 删除订单]: failed: %v", err)
			return nil, err
		}
		// 删除 order 表中数据
		_, err = s.In("id", ids).Delete(o)
		if err != nil {
			log.Printf("[db 删除订单]: failed: %v", err)
			return nil, err
		}
		return nil, nil
	})
	if err != nil {
		log.Printf("[db 删除订单]: failed: %v", err)
		return err
	}
	return nil
}
