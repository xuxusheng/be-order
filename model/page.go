package model

type DataWithPage struct {
	Pn    int
	Ps    int
	Total int
	Data  interface{}
}
