package resolver

import (
	"github.com/graphql-go/graphql"
	"log"
)

var createHelloField = &graphql.Field{
	Type:    graphql.Boolean,
	Resolve: createHello,
}

var queryHelloField = &graphql.Field{
	Description: "hello world",
	Type:        graphql.String,
	Resolve:     queryHello,
}

func createHello(p graphql.ResolveParams) (interface{}, error) {
	return true, nil
}

func queryHello(p graphql.ResolveParams) (interface{}, error) {
	rootValue, ok := p.Info.RootValue.(map[string]interface{})
	if !ok {
		log.Printf("rootValue 类型推断错误：%v", ok)
	}
	name, ok := rootValue["name"].(string)
	if !ok {
		log.Printf("rootValue.name 类型推断错误：%v", ok)
	}
	return "hello " + name, nil
}
