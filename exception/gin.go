package exception

import (
	"errors"
	"github.com/gin-gonic/gin"
	"net/http"
)

type ErrMeta struct {
	ErrCode    int
	StatusCode int
}

// 以下错误，给非 graphql 请求使用，直接在 gin 的 controller 里调用 c.Error() 并 return 即可。

func TokenErr(msg string) *gin.Error {
	return &gin.Error{
		Err:  errors.New(msg),
		Type: gin.ErrorTypePublic,
		Meta: &ErrMeta{
			StatusCode: http.StatusBadRequest,
			ErrCode:    TokenErrCode,
		},
	}
}

func ServerErr(msg string) *gin.Error {
	return &gin.Error{
		Err:  errors.New(msg),
		Type: gin.ErrorTypePublic,
		Meta: &ErrMeta{
			StatusCode: http.StatusInternalServerError,
			ErrCode:    ServerErrCode,
		},
	}
}
