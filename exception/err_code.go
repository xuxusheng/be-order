package exception

const (
	ServerErrCode     = 2007501
	TokenErrCode      = 2007102
	BadRequestErrCode = 2007202
)
