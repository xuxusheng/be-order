package middleware

import (
	"bytes"
	"fmt"
	"git.avlyun.org/inf/go-pkg/gateway"
	"gitee.com/xuxusheng/be-order/exception"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httputil"
	"runtime"
	"time"
)

var (
	dunno     = []byte("???")
	centerDot = []byte("·")
	dot       = []byte(".")
	slash     = []byte("/")
	reset     = string([]byte{27, 91, 48, 109})
)

// 错误处理中间件
func ResponseMiddleware() gin.HandlerFunc {
	return recoveryWithWriter(gin.DefaultErrorWriter)
}

func recoveryWithWriter(out io.Writer) gin.HandlerFunc {
	var logger *log.Logger
	if out != nil {
		logger = log.New(out, "\n\n\x1b[31m", log.LstdFlags)
	}
	return func(c *gin.Context) {
		resp := gateway.FormattedResp{}
		requestId, err := uuid.NewRandom()
		if err != nil {
			resp.Meta.ReqId = ""
			resp.Meta.ErrMsg = "super big exception"
			c.AbortWithStatusJSON(http.StatusInternalServerError, resp)
			return
		}
		defer func() {
			if err := recover(); err != nil {
				if logger != nil {
					stack := stack(3)
					httprequest, _ := httputil.DumpRequest(c.Request, false)
					logger.Printf("[Recovery] %s panic recovered:\n%s\n%s\n%s%s", timeFormat(time.Now()), string(httprequest), err, stack, reset)
				}
				c.AbortWithStatusJSON(http.StatusInternalServerError, resp)
			}
		}()

		resp.Meta.ReqId = requestId.String()
		c.Set("request_id", requestId)

		// Next
		c.Next()

		// 处理 gin 抛出的错误
		if c.Writer.Status() != http.StatusOK {
			statusCode := c.Writer.Status()
			var errMsg string
			if statusCode == 404 {
				errMsg = "404 page not found"
			} else if statusCode == 405 {
				errMsg = "405 method not allowed"
			} else {
				errMsg = "not err description"
			}
			resp.Meta.ErrCode = 0
			resp.Meta.ErrMsg = errMsg
			c.AbortWithStatusJSON(statusCode, resp)
			return
		}

		statusCode := http.StatusOK
		if c.Errors.Last() != nil {
			ginErr := c.Errors.Last()
			ginMeta := ginErr.Meta
			log.Printf("出错：%v", ginErr.Error())
			if errMeta, ok := ginMeta.(*exception.ErrMeta); ok {
				resp.Meta.ErrCode = uint(errMeta.ErrCode)
				resp.Meta.ErrMsg = ginErr.Error()
				statusCode = errMeta.StatusCode
			} else {
				resp.Meta.ErrCode = 0
				resp.Meta.ErrMsg = "Internal Error!"
				statusCode = http.StatusInternalServerError
			}
		}

		if data, exists := c.Get("data"); exists {
			resp.SetData(data)
		}
		c.JSON(statusCode, resp)

	}
}

// 以下代码都是从 Gin 抄的
// stack returns a nicely formatted stack frame, skipping skip frames.
func stack(skip int) []byte {
	buf := new(bytes.Buffer) // the returned data
	// As we loop, we open files and read them. These variables record the currently
	// loaded file.
	var lines [][]byte
	var lastFile string
	for i := skip; ; i++ { // Skip the expected number of frames
		pc, file, line, ok := runtime.Caller(i)
		if !ok {
			break
		}
		// Print this much at least.  If we can't find the source, it won't show.
		fmt.Fprintf(buf, "%s:%d (0x%x)\n", file, line, pc)
		if file != lastFile {
			data, err := ioutil.ReadFile(file)
			if err != nil {
				continue
			}
			lines = bytes.Split(data, []byte{'\n'})
			lastFile = file
		}
		fmt.Fprintf(buf, "\t%s: %s\n", function(pc), source(lines, line))
	}
	return buf.Bytes()
}

// source returns a space-trimmed slice of the n'th line.
func source(lines [][]byte, n int) []byte {
	n-- // in stack trace, lines are 1-indexed but our array is 0-indexed
	if n < 0 || n >= len(lines) {
		return dunno
	}
	return bytes.TrimSpace(lines[n])
}

// function returns, if possible, the name of the function containing the PC.
func function(pc uintptr) []byte {
	fn := runtime.FuncForPC(pc)
	if fn == nil {
		return dunno
	}
	name := []byte(fn.Name())
	// The name includes the path name to the package, which is unnecessary
	// since the file name is already included.  Plus, it has center dots.
	// That is, we see
	//	runtime/debug.*T·ptrmethod
	// and want
	//	*T.ptrmethod
	// Also the package path might contains dot (e.g. code.google.com/...),
	// so first eliminate the path prefix
	if lastslash := bytes.LastIndex(name, slash); lastslash >= 0 {
		name = name[lastslash+1:]
	}
	if period := bytes.Index(name, dot); period >= 0 {
		name = name[period+1:]
	}
	name = bytes.Replace(name, centerDot, dot, -1)
	return name
}
func timeFormat(t time.Time) string {
	var timeString = t.Format("2006/01/02 - 15:04:05")
	return timeString
}
