package model

import (
	"log"
	"time"
)

// 电话号码表
type Phone struct {
	Id         int64
	Tel        string    `xorm:"notnull"` // 号码
	CustomerId int64     `xorm:"notnull"` // 客户 ID
	CreatedAt  time.Time `xorm:"created"`
	UpdatedAt  time.Time `xorm:"updated"`
}

type PhoneResult struct {
	Id  int64
	Tel string `json:"tel"`
}

func (p *Phone) Create() (int64, error) {
	affected, err := GetDB().Insert(p)
	if err != nil {
		log.Printf("[db 创建电话] failed：%v", err)
		return 0, err
	}
	return affected, nil
}

func (p *Phone) Delete() error {
	_, err := GetDB().ID(p.Id).Delete(p)
	if err != nil {
		log.Printf("[db 删除电话] failed: %v", err)
		return err
	}
	return nil
}

func (p *Phone) DeleteMulti(ids []int) error {
	_, err := GetDB().In("id", ids).Delete(p)
	if err != nil {
		log.Printf("[db 删除电话] failed: %v", err)
		return err
	}
	return nil
}

func (p *Phone) Update() error {
	_, err := GetDB().Table(p).ID(p.Id).Update(map[string]interface{}{
		"tel": p.Tel,
	})
	if err != nil {
		log.Printf("[db 更新电话] failed：%v", err)
		return err
	}
	return nil
}

//
//func (p *PhoneResult) CreateMulti(customerId int, tels []string) error {
//	// 批量添加
//	tx := GetDB().Begin()
//	for _, t := range tels {
//		err := tx.FirstOrCreate(&Phone{Tel: t, CustomerId: customerId}).Error
//		if err != nil {
//			log.Printf("创建 phone 失败：%v", err)
//			tx.Rollback()
//			return err
//		}
//	}
//	tx.Commit()
//	return nil
//}
