package exception

import (
	"github.com/graphql-go/graphql/gqlerrors"
	"net/http"
)

type Error struct {
	Message   string
	Extension map[string]interface{}
}

func (e *Error) Error() string {
	return e.Message
}

func (e *Error) Extensions() map[string]interface{} {
	return e.Extension
}

func New(statusCode, errCode int, msg string) gqlerrors.ExtendedError {
	return &Error{
		Message: msg,
		Extension: map[string]interface{}{
			"status":  statusCode,
			"errCode": errCode,
		},
	}
}

func BadRequestError(msg string) gqlerrors.ExtendedError {
	return &Error{
		Message: msg,
		Extension: map[string]interface{}{
			"status":  http.StatusBadRequest,
			"errCode": BadRequestErrCode,
		},
	}
}
