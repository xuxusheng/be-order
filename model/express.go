package model

import "time"

// 物流订单表
type Express struct {
	Id        int64
	OrderId   int       `xorm:"notnull"` // 订单ID
	Code      int       `xorm:"notnull"` // 物流订单单号
	CreatedAt time.Time `xorm:"created"`
	UpdatedAt time.Time `xorm:"updated"`
}

type ExpressResult struct {
	Id        int   `json:"id"`
	Code      int   `json:"code"`
	CreatedAt int64 `json:"created_at"`
	UpdatedAt int64 `json:"updated_at"`
}
