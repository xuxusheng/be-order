package gateway

import (
	"encoding/base64"
	"encoding/json"
	"errors"
	"net/http"
	"strings"
)

type BackendMeta struct {
	ErrMsg   string `json:"err_msg"`
	ErrCode  uint   `json:"err_code"`
	ReqId    string `json:"req_id"`
	HitCache bool   `json:"hits"`
}
type BackendMetaCompatible struct {
	AErrMsg   string `json:"err_msg"`
	AErrCode  uint   `json:"err_code"`
	AReqId    string `json:"req_id"`
	AHits     bool   `json:"hits"`
	OErrMsg   string `json:"errMsg"`
	OErrCode  uint   `json:"errCode"`
	OReqId    string `json:"reqId"`
	OHitCache bool   `json:"hitCache"`
}
type BackendResp struct {
	Meta BackendMeta `json:"meta"`
	Data interface{} `json:"data"`
}

type FormattedResp struct {
	Meta BackendMeta   `json:"meta"`
	More []BackendMeta `json:"more"`
	Data interface{}   `json:"data"`
}

func (f *FormattedResp) SetData(d interface{}) {
	f.Data = d
}

type Token struct {
	Email  string `json:"dsp"`
	Expire uint64 `json:"exp"`
	Id     uint   `json:"sub"`
}

func ParseUId(req *http.Request) (uint, error) {
	tokenStr := req.Header.Get("authorization")
	if tokenStr == "" {
		return 0, errors.New("anonymous")
	}
	part := strings.Split(tokenStr, ".")
	if len(part) < 2 {
		return 0, errors.New("illegal token, too short")
	}
	b, err := base64.RawStdEncoding.DecodeString(part[1])
	if err != nil {
		return 0, err
	}
	token := Token{}
	err = json.Unmarshal(b, &token)
	if err != nil {
		return 0, err
	}
	return token.Id, nil
}

func (b *BackendMeta) UnmarshalJSON(data []byte) (err error) {
	t := BackendMetaCompatible{}
	err = json.Unmarshal(data, &t)
	if err != nil {
		return err
	}
	b.HitCache = t.AHits || t.OHitCache
	if t.AReqId != "" {
		b.ReqId = t.AReqId
	} else {
		b.ReqId = t.OReqId
	}
	if t.AErrMsg != "" {
		b.ErrMsg = t.AErrMsg
	} else {
		b.ErrMsg = t.OErrMsg
	}
	b.ErrCode = t.AErrCode | t.OErrCode

	return
}
