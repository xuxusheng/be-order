package form

type OrderForm struct {
	CustomerName    string `json:"customer_name" binding:"required"`    // 客户姓名
	CustomerPhone   string `json:"customer_phone" binding:"required"`   // 客户电话
	CustomerAddress string `json:"customer_address" binding:"required"` // 客户地址

}
