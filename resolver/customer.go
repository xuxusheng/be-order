package resolver

import (
	"fmt"
	"gitee.com/xuxusheng/be-order/model"
	"github.com/graphql-go/graphql"
	"log"
)

var Customer = graphql.NewObject(graphql.ObjectConfig{
	Name: "Customer",
	Fields: graphql.Fields{
		"id": &graphql.Field{
			Description: "顾客ID",
			Type:        graphql.Int,
		},
		"name": &graphql.Field{
			Description: "顾客姓名",
			Type:        graphql.String,
		},
		"nick": &graphql.Field{
			Description: "顾客昵称",
			Type:        graphql.String,
		},
		"phone": &graphql.Field{
			Description: "顾客的电话信息",
			Type:        graphql.NewList(phone),
		},
		"address": &graphql.Field{
			Description: "顾客的地址信息",
			Type:        graphql.NewList(address),
		},
		"createdAt": &graphql.Field{
			Description: "创建时间",
			Type:        graphql.Int,
		},
		"updatedAt": &graphql.Field{
			Description: "更新时间",
			Type:        graphql.Int,
		},
	},
})

var CustomerWithPage = graphql.NewObject(graphql.ObjectConfig{
	Name: "CustomerWithPage",
	Fields: graphql.Fields{
		"pn": &graphql.Field{
			Description: "第几页",
			Type:        graphql.Int,
		},
		"ps": &graphql.Field{
			Description: "每页多少条数据",
			Type:        graphql.Int,
		},
		"total": &graphql.Field{
			Description: "总共多少条数据",
			Type:        graphql.Int,
		},
		"data": &graphql.Field{
			Description: "顾客信息列表",
			Type:        graphql.NewList(Customer),
		},
	},
})

var createCustomerField = &graphql.Field{
	Description: "创建客户",
	Type:        graphql.Boolean,
	Args: graphql.FieldConfigArgument{
		"name": &graphql.ArgumentConfig{
			Description: "客户姓名",
			Type:        graphql.NewNonNull(graphql.String),
		},
		"nick": &graphql.ArgumentConfig{
			Description:  "客户昵称",
			Type:         graphql.String,
			DefaultValue: "",
		},
		"phone": &graphql.ArgumentConfig{
			Description: "客户电话",
			Type:        graphql.NewNonNull(graphql.NewList(graphql.String)),
		},
		"address": &graphql.ArgumentConfig{
			Description: "客户地址",
			Type:        graphql.NewNonNull(graphql.NewList(graphql.String)),
		},
	},
	Resolve: createCustomer,
}

var deleteCustomerField = &graphql.Field{
	Description: "删除客户",
	Type:        graphql.Boolean,
	Args: graphql.FieldConfigArgument{
		"ids": &graphql.ArgumentConfig{
			Description: "客户ID列表",
			Type:        graphql.NewNonNull(graphql.NewList(graphql.NewNonNull(graphql.Int))),
		},
	},
	Resolve: deleteCustomer,
}

var updateCustomerField = &graphql.Field{
	Description: "更新客户信息",
	Type:        graphql.Boolean,
	Args: graphql.FieldConfigArgument{
		"id": &graphql.ArgumentConfig{
			Description: "客户ID",
			Type:        graphql.NewNonNull(graphql.Int),
		},
		"name": &graphql.ArgumentConfig{
			Description: "客户姓名",
			Type:        graphql.NewNonNull(graphql.String),
		},
		"nick": &graphql.ArgumentConfig{
			Description:  "客户昵称",
			Type:         graphql.String,
			DefaultValue: "",
		},
		"phone": &graphql.ArgumentConfig{
			Description: "客户电话列表",
			Type:        graphql.NewNonNull(graphql.NewList(graphql.NewNonNull(graphql.String))),
		},
		"address": &graphql.ArgumentConfig{
			Description: "客户地址列表",
			Type:        graphql.NewNonNull(graphql.NewList(graphql.NewNonNull(graphql.String))),
		},
	},
	Resolve: updateCustomer,
}

var queryCustomerField = &graphql.Field{
	Description: "查询客户列表",
	Type:        CustomerWithPage,
	Args: graphql.FieldConfigArgument{
		"query": &graphql.ArgumentConfig{
			Description:  "查询条件",
			Type:         graphql.String,
			DefaultValue: "",
		},
		"pn": &graphql.ArgumentConfig{
			Description:  "第几页",
			Type:         graphql.Int,
			DefaultValue: 1,
		},
		"ps": &graphql.ArgumentConfig{
			Description:  "每页显示多少条",
			Type:         graphql.Int,
			DefaultValue: 10,
		},
	},
	Resolve: queryCustomer,
}

func createCustomer(p graphql.ResolveParams) (interface{}, error) {
	name := p.Args["name"].(string)
	nick := p.Args["nick"].(string)
	phone := p.Args["phone"].([]interface{})
	address := p.Args["address"].([]interface{})

	var phoneSlice []string
	for _, v := range phone {
		phoneSlice = append(phoneSlice, v.(string))
	}
	var addressSlice []string
	for _, v := range address {
		addressSlice = append(addressSlice, v.(string))
	}

	customer := model.Customer{
		Name: name,
		Nick: nick,
	}
	affected, err := customer.Create(phoneSlice, addressSlice)
	fmt.Printf("affected: %v", affected)
	if err != nil {
		log.Printf("[resolver 创建客户] failed：%v", err)
		return false, err
	}
	return true, nil
}

func deleteCustomer(p graphql.ResolveParams) (interface{}, error) {
	ids := p.Args["ids"].([]interface{})

	var iIds []int
	for _, v := range ids {
		iIds = append(iIds, v.(int))
	}

	err := (&model.Customer{}).DeleteMulti(iIds)
	if err != nil {
		log.Printf("[resolver 删除客户] failed: %v", err)
		return false, err
	}
	return true, nil

}

func updateCustomer(p graphql.ResolveParams) (interface{}, error) {
	id := p.Args["id"].(int)
	name := p.Args["name"].(string)
	nick := p.Args["nick"].(string)
	phone := p.Args["phone"].([]interface{})
	address := p.Args["address"].([]interface{})

	var phones []string
	for _, v := range phone {
		phones = append(phones, v.(string))
	}

	var addresses []string
	for _, v := range address {
		addresses = append(addresses, v.(string))
	}

	c := model.Customer{
		Id:   int64(id),
		Name: name,
		Nick: nick,
	}
	err := c.Update(phones, addresses)
	if err != nil {
		log.Printf("[resolve 更新客户信息] failed: %v", err)
		return false, err
	}
	return true, nil
}

func queryCustomer(p graphql.ResolveParams) (interface{}, error) {
	query := p.Args["query"].(string)
	pn := p.Args["pn"].(int)
	ps := p.Args["ps"].(int)

	c := model.Customer{}
	r, total, err := c.All(query, pn, ps)
	if err != nil {
		log.Printf("[resolver 客户列表] failed: %v", err)
		return nil, err
	}

	return model.DataWithPage{
		Pn:    pn,
		Ps:    ps,
		Total: total,
		Data:  r,
	}, nil
}
