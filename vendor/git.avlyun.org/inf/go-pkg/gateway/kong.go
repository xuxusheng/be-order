package gateway

import (
	"git.avlyun.org/inf/go-pkg/logex"
	"github.com/gin-gonic/gin"
	"net/http"
	"net/http/httputil"
	"os"
)

var header = http.Header{}
var kongProxy *httputil.ReverseProxy

func init() {
	protocol := os.Getenv("PROTOCOL")
	if protocol == "" {
		protocol = "http"
	}

	allEnvKey := [][]string{[]string{"KONG_KEY", "apikey"},
		[]string{"KONG_HOST", "Host"},
		[]string{"LOG_CLIENT", "x-client"},
		[]string{"ACCESS_KEY", "x-access-key"}}
	for _, envKey := range allEnvKey {
		v := os.Getenv(envKey[0])
		if v != "" {
			header.Set(envKey[1], v)
		}
	}
	header.Set("Content-Type", "application/json")
	logex.Debug(header)

	director := func(req *http.Request) {
		req.URL.Scheme = protocol
		req.URL.Host = Proxy.getApi()
		//req.URL.Path = ""
		if _, ok := req.Header["User-Agent"]; !ok {
			// explicitly disable User-Agent so it's not set to default value
			req.Header.Set("User-Agent", "")
		}
		overwriteHeader(req.Header, header)
		logex.Debug(req.Header)
	}
	kongProxy = &httputil.ReverseProxy{Director: director}
}

func Kong(c *gin.Context) {
	kongProxy.ServeHTTP(c.Writer, c.Request)
}

func overwriteHeader(dst, src http.Header) {
	for k, vv := range src {
		for _, v := range vv {
			dst.Set(k, v)
		}
	}
}
