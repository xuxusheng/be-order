package resolver

import "github.com/graphql-go/graphql"

var rootQuery = graphql.ObjectConfig{
	Name: "RootQuery",
	Fields: graphql.Fields{
		"hello":    queryHelloField,
		"customer": queryCustomerField,
		"address":  queryAddressField,
	},
}

var rootMutation = graphql.ObjectConfig{
	Name: "RootMutation",
	Fields: graphql.Fields{
		"createHello": createHelloField,
		// 客户 增删改
		"createCustomer": createCustomerField,
		"deleteCustomer": deleteCustomerField,
		"updateCustomer": updateCustomerField,
		// 地址 增删改
		"createAddress": createAddressField,
		"deleteAddress": deleteAddressField,
		"updateAddress": updateAddressField,
		// 电话增删改
		"createPhone": createPhoneField,
		"deletePhone": deletePhoneField,
		"updatePhone": updatePhoneField,
	},
}

var SchemaConfig = graphql.SchemaConfig{
	Query:    graphql.NewObject(rootQuery),
	Mutation: graphql.NewObject(rootMutation),
}
