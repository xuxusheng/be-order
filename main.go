package main

import (
	"gitee.com/xuxusheng/be-order/controller"
	"gitee.com/xuxusheng/be-order/middleware"
	"gitee.com/xuxusheng/be-order/model"
	"github.com/gin-gonic/gin"
)

func main() {

	// 连接数据库
	model.InitDB()

	r := gin.Default()

	r.Use(
		middleware.TestMiddleware(),
		middleware.ResponseMiddleware(),
	)

	r.Any("/apps/apple-order/graph", controller.GraphHandler)

	r.Run()
}
