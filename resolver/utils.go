package resolver

import "github.com/graphql-go/graphql"

func fieldWithPage(field *graphql.Object, description string) *graphql.Object {
	return graphql.NewObject(graphql.ObjectConfig{
		Description: "带分页的" + description,
		Name:        field.Name() + "WithPage",
		Fields: graphql.Fields{
			"pn": &graphql.Field{
				Description: "第几页",
				Type:        graphql.Int,
			},
			"ps": &graphql.Field{
				Description: "每页多少条数据",
				Type:        graphql.Int,
			},
			"total": &graphql.Field{
				Description: "总共多少条数据",
				Type:        graphql.Int,
			},
			"data": &graphql.Field{
				Description: description,
				Type:        graphql.NewList(field),
			},
		},
	})
}
