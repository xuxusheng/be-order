package main

import "fmt"

type Person struct {
	Id   int
	Name string
}

type List struct {
	Person []Person
}

func main() {

	lists := []List{
		{
			Person: []Person{
				{1, "xusheng"},
				{1, "xusheng"},
				{2, "xs"},
				{2, "xs"},
			},
		},
		{
			Person: []Person{
				{1, "xusheng"},
				{1, "xusheng"},
				{2, "xs"},
				{2, "xs"},
			},
		},
	}

	for _, v := range lists {

		v.Person = unquie(v.Person)

		fmt.Println(&v == &lists[0])
	}

	fmt.Printf("%+v", lists)

}

func unquie(s []Person) []Person {

	var result []Person
	tmp := map[int]Person{}

	for _, v := range s {
		tmp[v.Id] = v
	}

	for _, v := range tmp {
		result = append(result, v)
	}

	return result
}
