package resolver

import (
	"gitee.com/xuxusheng/be-order/model"
	"github.com/graphql-go/graphql"
	"log"
)

var address = graphql.NewObject(graphql.ObjectConfig{
	Description: "地址信息",
	Name:        "Address",
	Fields: graphql.Fields{
		"id": &graphql.Field{
			Description: "地址信息ID",
			Type:        graphql.Int,
		},
		"postCode": &graphql.Field{
			Description: "邮政编码",
			Type:        graphql.String,
		},
		"address": &graphql.Field{
			Description: "地址",
			Type:        graphql.String,
		},
	},
})

var addressWithPage = fieldWithPage(address, "地址信息列表")

var createAddressField = &graphql.Field{
	Description: "创建地址",
	Type:        graphql.Boolean,
	Args: graphql.FieldConfigArgument{
		"customerId": &graphql.ArgumentConfig{
			Description: "客户ID",
			Type:        graphql.NewNonNull(graphql.Int),
		},
		"address": &graphql.ArgumentConfig{
			Description: "地址",
			Type:        graphql.NewNonNull(graphql.String),
		},
	},
	Resolve: createAddress,
}

var deleteAddressField = &graphql.Field{
	Description: "删除地址",
	Type:        graphql.Boolean,
	Args: graphql.FieldConfigArgument{
		"ids": &graphql.ArgumentConfig{
			Description: "地址ID数组",
			Type:        graphql.NewNonNull(graphql.NewList(graphql.NewNonNull(graphql.Int))),
		},
	},
	Resolve: deleteAddress,
}

var updateAddressField = &graphql.Field{
	Description: "更新地址",
	Type:        graphql.Boolean,
	Args: graphql.FieldConfigArgument{
		"id": &graphql.ArgumentConfig{
			Description: "地址ID",
			Type:        graphql.NewNonNull(graphql.Int),
		},
		"address": &graphql.ArgumentConfig{
			Description: "地址",
			Type:        graphql.NewNonNull(graphql.String),
		},
	},
	Resolve: updateAddress,
}

var queryAddressField = &graphql.Field{
	Description: "地址列表",
	Type:        addressWithPage,
	Args: graphql.FieldConfigArgument{
		"pn": &graphql.ArgumentConfig{
			Description:  "第几页",
			Type:         graphql.Int,
			DefaultValue: 1,
		},
		"ps": &graphql.ArgumentConfig{
			Description:  "每页显示多少条",
			Type:         graphql.Int,
			DefaultValue: 10,
		},
		"query": &graphql.ArgumentConfig{
			Description:  "筛选条件",
			Type:         graphql.String,
			DefaultValue: "",
		},
	},
	Resolve: queryAddress,
}

func createAddress(p graphql.ResolveParams) (interface{}, error) {
	customerId := p.Args["customerId"].(int)
	address := p.Args["address"].(string)
	a := model.Address{
		CustomerId: int64(customerId),
		Address:    address,
	}
	err := a.Create()
	if err != nil {
		log.Printf("[resovler 创建地址] failed: %v", err)
		return false, err
	}
	return true, nil
}

func deleteAddress(p graphql.ResolveParams) (interface{}, error) {
	ids := p.Args["ids"].([]interface{})
	var iIds []int
	for _, v := range ids {
		iIds = append(iIds, v.(int))
	}

	a := model.Address{}
	err := a.DeleteMulti(iIds)
	if err != nil {
		log.Printf("[resolver 删除地址] failed: %v", err)
		return false, err
	}
	return true, nil
}

func updateAddress(p graphql.ResolveParams) (interface{}, error) {
	id := p.Args["id"].(int)
	address := p.Args["address"].(string)

	a := model.Address{
		Id:      int64(id),
		Address: address,
	}
	err := a.Update()
	if err != nil {
		log.Printf("[resolver 更新地址] failed: %v", err)
		return false, err
	}
	return true, nil
}

func queryAddress(p graphql.ResolveParams) (interface{}, error) {
	query := p.Args["query"].(string)
	pn := p.Args["pn"].(int)
	ps := p.Args["ps"].(int)

	a := model.Address{}
	results, total, err := a.Query(query, pn, ps)
	if err != nil {
		log.Printf("[resolver 查询地址] failed: %v", err)
		return nil, err
	}
	return model.DataWithPage{
		Pn:    pn,
		Ps:    ps,
		Total: total,
		Data:  results,
	}, nil
}
