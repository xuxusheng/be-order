package gateway

import (
	"bytes"
	"compress/gzip"
	"encoding/json"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"sync/atomic"
)

type proxy struct {
	api          []string
	pollingIndex int32
	protocol     string
	header       http.Header
}

func (p *proxy) Get() {
}

func (p *proxy) Request(method string, url string, body interface{}, upstream *http.Request) ([]byte, int, error) {
	b := make([]byte, 0)
	var err error
	if body != nil {
		b, err = json.Marshal(body)
		if err != nil {
			return nil, http.StatusBadRequest, err
		}
	}
	url = p.protocol + "://" + p.getApi() + url
	client := &http.Client{}
	req, err := http.NewRequest(method, url, bytes.NewBuffer(b))
	if err != nil {
		return nil, http.StatusInternalServerError, err
	}
	//proxy_set_header        x-forwarded-for     $proxy_add_x_forwarded_for;
	//proxy_set_header        x-request-id        $http_x_request_id;
	copyHeader(req.Header, upstream.Header)

	for k, _ := range p.header {
		req.Header.Set(k, p.header.Get(k))
	}
	resp, err := client.Do(req)

	if err != nil {
		return nil, http.StatusInternalServerError, err
	}

	var reader io.ReadCloser
	switch resp.Header.Get("content-encoding") {
	case "gzip":
		reader, err = gzip.NewReader(resp.Body)
	default:
		reader = resp.Body
	}

	b, err = ioutil.ReadAll(reader)
	if err != nil {
		return nil, http.StatusInternalServerError, err
	}

	return b, resp.StatusCode, nil
}

func (p *proxy) getApi() string {
	atomic.AddInt32(&p.pollingIndex, 1)
	return p.api[p.pollingIndex%int32(len(p.api))]
}

var Proxy proxy

func init() {
	Restore()
}

// just for test
func AddApi(apis ...string) {
	Proxy.api = append(Proxy.api, apis...)
}

// just for test
func CleanApi() {
	Proxy.api = make([]string, 0)
}

// just for test
func SetProto(p string) {
	Proxy.protocol = p
}

func Restore() {
	api := strings.Split(os.Getenv("API"), ",")
	for index, value := range api {
		api[index] = strings.TrimSpace(value)
	}
	protocol := os.Getenv("PROTOCOL")
	if protocol == "" {
		protocol = "http"
	}
	header := http.Header{}
	allEnvKey := [][]string{[]string{"KONG_KEY", "apikey"},
		[]string{"KONG_HOST", "Host"},
		[]string{"LOG_CLIENT", "x-client"},
		[]string{"ACCESS_KEY", "x-access-key"}}
	for _, envKey := range allEnvKey {
		v := os.Getenv(envKey[0])
		if v != "" {
			header.Set(envKey[1], v)
		}
	}
	header.Set("Content-Type", "application/json")
	Proxy = proxy{api, 0, protocol, header}
}
func copyHeader(dst, src http.Header) {
	for k, vv := range src {
		for _, v := range vv {
			dst.Add(k, v)
		}
	}
}
